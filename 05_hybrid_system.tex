\chapter{Hybrid System} \label{chapter:hybrid}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% HYBRID SYSTEM DESCRIPTION
\section{Hybrid System Description} \label{sec:hybrid_description}

As the introduction mentions, I will incorporate three modes describing different ball motions on the beam. The three separate modes signify creating a system that contains discrete-valued signals with if-then-else conditions and also involves real-valued signals. Such a system is called a \textit{hybrid system} and describes dynamical interactions between continuous and discrete signals in one common framework \cite{Hybrid}. \par
The modes, which represent the components of the hybrid system, are namely a \textit{balancing mode}, a \textit{revolving mode}, and a \textit{projectile mode}. All three modes are portrayed in Figure \ref{fig:modes}. The aim is to create a model of the hybrid system, which will be able to transition between these modes depending on the ball's position in relation to the beam. Figure \ref{fig:hybrid} exhibits a schematic diagram comprising all three modes of the model, with index $i$ denoting a specific mode and $\gamma_{ij}$ signifying a guard condition for transitioning from the current mode $i$ to the next mode $j$. All three modes will be delineated now in detail below. To describe the hybrid system, I distinguish two parts of the beam: the flat part of length $d_1$ and the rounded part of radius $r_1$. 
\begin{figure}[htb]
    \centering
    \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[trim=10 10 10 10, clip, width=4cm]{./img/PM.pdf}
        \captionsetup{justification=centering}
        \caption{Projectile mode.}
        \label{fig:mode3}
    \end{subfigure}%
    \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[trim=10 10 10 10, clip, width=4cm]{./img/BM.pdf}
        \captionsetup{justification=centering}
        \caption{Balancing mode.}
        \label{fig:mode1}
    \end{subfigure}%
    \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[trim=10 10 10 10, clip, width=4cm]{./img/RM.pdf}
        \captionsetup{justification=centering}
        \caption{Revolving mode.}
        \label{fig:mode2}
    \end{subfigure}
    \captionsetup{justification=centering}
    \caption{Three modes forming the hybrid system.}
    \label{fig:modes}
\end{figure} \noindent
\begin{figure}[htb]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=11cm]{./img/hybrid_system.pdf}
    \captionsetup{justification=centering}
    \caption{Diagram of the hybrid system, including the guard conditions.}
    \label{fig:hybrid}
\end{figure}
\begin{itemize}
    \item{\textbf{Balancing mode} -- This is the fundamental mode of the hybrid system, where the goal is to balance the ball anywhere on the flat part of the beam. Depending on the RaspiCam positional output of the ball, the motor with the beam tilts and moves the ball into the desired position. \par
    The system always begins with this mode on start-up and then, if desired, changes the mode to one of the other two. The system enters this mode before and after the revolving, and the projectile mode run.}
    \item{\textbf{Revolving mode} -- This mode commences with a slight beam tilt with two possible scenarios when the ball approaches one of the beam's ends. The first option is that the beam makes a half rotation ($\pi$ rotation), and the ball simultaneously traverses the rounded part and reaches the other flat part of the beam. The balancing mode is activated afterward to balance the ball on the beam. The second option is to balance the ball on the rounded part in the beam's vertical position. \par
    It is essential that the ball stays in contact with the beam and does not leave the groove throughout any of the two motions.}
    \item{\textbf{Projectile mode} -- With a rapid beam tilt, the beam projects the ball, starting a free-fall motion. The beam returns to its horizontal position. Before the ball touches the beam, the beam slightly tilts in the same direction as the landing ball to dampen the fall. The balancing mode is then activated to balance the ball. \par
    The ball can be projected from any point on the beam. The landing point on the flat part of the beam is determined based on the tilt causing the projectile motion.}
\end{itemize}
Derivations of the model motion equations will be described in Sections \ref{sec:balancing}, \ref{sec:revolving}, and \ref{sec:projectile}. I will use the \textit{Euler-Lagrange formalism} to derive the equations and model the individual modes. Before delving into the derivations, I would like to break down the selection of the coordinate systems in which the models will be described. \par
Firstly, polar coordinates are an obvious choice for the revolving mode, as the mode represents a circular motion. The polar coordinates will also be used for the balancing mode. Although expressing all three modes in the same coordinate systems would be convenient, Cartesian coordinates are more suitable for the projectile mode because the mode's motion equations will be linear. With this choice of coordinates, one has to reconcile himself to a transformation of coordinates when transitioning between two modes with different coordinates. The transformations will be described in Section \ref{sec:transformation}. \par
To simplify the model equations, I will not state the time dependence of generalized coordinates. Also, I will not display the angle unit $\si{\radian}$ for enumerated angles for the rest of the text. \par
For a beam moment of inertia $I_1$, I assume the beam to be a bar of length $l_1$ and mass $m_1$ with the axis of rotation going through the center pivot of the bar. For the ball, I consider a moment of inertia $I_2$ of a homogeneous sphere with radius $r_2$ and mass $m_2$ with the axis of rotation going through the sphere's center of gravity. The formulas and values for both moments are
\begin{align}
    \label{eqn:moment1}
    I_1 &= \frac{1}{12}m_1 l_1^2 = 2.220 \cdot 10^{-4}\,\si{\kilo\gram\metre\squared}\,, \\
    \label{eqn:moments_vals}
    I_2 &= \frac{2}{5}m_2 r_2^2 = 1.308 \cdot 10^{-6}\,\si{\kilo\gram\metre\squared}\,.
\end{align}
Having both estimated and calculated the beam moment of inertia, I can now draw a comparison between the estimated (\ref{eqn:ident_params}) and the computed (\ref{eqn:moments_vals}) value. One can see that the moments' orders correspond; the estimated value is, however, four times bigger. For the following computations, I will consider the moment of inertia discovered with the Grey-Box Model Estimation as the formula (\ref{eqn:moments_vals}) is only a theoretical approximation neglecting the exact dynamics of the bar.

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% BALANCING MODE
\section{Balancing Mode} \label{sec:balancing}

In this section, I will derive the motion equations for balancing the ball on the beam based on \cite{BaB_model}. I choose length $d$ and angle $\theta$ as generalized coordinates, resulting in $\mathbf{q} = \begin{pmatrix} \theta & d \end{pmatrix}^\top$. The coordinate $d$ corresponds to the distance of the ball center to a position where the ball is situated in the middle of the beam's flat part. Angle $\theta$ is the angle of the beam w.r.t. the horizontal axis of the world frame. The coordinates are shown in Figure \ref{fig:bab}. \par
The kinetic co-energy is
\begin{equation}
    T^* = \frac{1}{2}I_1\dot{\theta}^2 + \frac{1}{2}m_2(d^2\dot{\theta}^2 + \dot{d}^2) + \frac{1}{2}I_2(\dot{\theta} + \dot{\varphi})^2\,.
\end{equation}
A rotation of the ball $\varphi$ can be expressed as 
\begin{equation}
    \label{eqn:ball_rot}
    \varphi = \frac{d}{r_2} + \theta\,.
\end{equation}   
The potential energy is given by 
\begin{equation}
    V = m_2\mathrm{g}h\,,
\end{equation}
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=9cm]{./img/balancing_ball.pdf}
    \captionsetup{justification=centering}
    \caption{Balancing the ball on the beam denoted with the generalized coordinates $\theta$ and $d$ and the moments of inertia $I_1$ and $I_2$.}
    \label{fig:bab}
\end{figure} \noindent 
where
\begin{equation}
    h = d\sin(\theta) + (r_1 + r_2)\cos(\theta)\,.
\end{equation}
The dissipation function of the ball and the beam is
\begin{equation}
    D = \frac{1}{2}b_1\dot{\theta}^2 + \frac{1}{2}b_2\dot{\varphi}^2\,.
\end{equation}
Considering the Lagrangian
\begin{equation}
    \label{eqn:lagrangian}
    \mathcal{L} = T^* - V\,,
\end{equation}
I can now write the Euler-Lagrange equations
\begin{align}
    \label{eqn:balancing1}
    \dv{}{t}\frac{\partial \mathcal{L}}{\partial \dot{\theta}} - \frac{\partial \mathcal{L}}{\partial \theta} + \frac{\partial D}{\partial \dot{\theta}} &= \tau\,, \\
    \label{eqn:balancing2}
    \dv{}{t}\frac{\partial \mathcal{L}}{\partial \dot{d}} - \frac{\partial \mathcal{L}}{\partial d} + \frac{\partial D}{\partial \dot{d}} &= 0\,,
\end{align}
where $\tau$ represents the external torque applied to the beam from the motor. I will not state the differential equations directly as I think expressing the equations in a matrix form will be more apparent. For this purpose, I use a dynamic equation
\begin{equation}
    \mathbf{M}(\mathbf{q})\ddot{\mathbf{q}} + \mathbf{C}(\mathbf{q},\dot{\mathbf{q}})\dot{\mathbf{q}} + \mathbf{G}(\mathbf{q}) = \mathbf{Q}\,,
\end{equation}
where $\mathbf{M}$ is the inertia matrix, $\mathbf{C}$ is the Coriolis matrix, $\mathbf{G}$ is the gravity vector, and $\mathbf{Q}$ is the vector of external forces. \par
Substituting the equations (\ref{eqn:balancing1}) and (\ref{eqn:balancing2}) into the matrices, one gets
\begin{align}
    \begin{split}
    \label{eqn:BM_matrices}
        \mathbf{M}(\mathbf{q}) &= 
        \begin{bmatrix}
            I_1 + 4I_2 + m_2d^2 & 2\dfrac{I_2}{r_2} \\
            2\dfrac{I_2}{r_2} & \dfrac{I_2}{r_2^2} + m_2
        \end{bmatrix}\,, \\
        \mathbf{C}(\mathbf{q},\dot{\mathbf{q}}) &=
        \begin{bmatrix}
            b_1 + b_2 + 2m_2\dot{d}d & \dfrac{b_2}{r_2} \\
            \dfrac{b_2}{r_2} - m_2\dot{\theta}d & \dfrac{b_2}{r_2^2}
        \end{bmatrix}\,, \\
        \mathbf{G}(\mathbf{q}, \dot{\mathbf{q}}) &=
        \begin{bmatrix}
            m_2\mathrm{g}d\cos(\theta) - m_2\mathrm{g}(r_1 + r_2)\sin(\theta) \\
            m_2\mathrm{g}\sin(\theta)
        \end{bmatrix}\,, \\
        \mathbf{Q} &=
        \begin{bmatrix}
            1 \\
            0
        \end{bmatrix}\tau\,.
    \end{split}
\end{align}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% LINEARIZATION
\subsection{Balancing Mode Linearization}

Unlike the projectile mode, the balancing mode's equations are nonlinear. This means that the mode linearization in a relevant equilibrium point is required to design a control for the system in this mode. The linear model will be described using a state-space representation of the form
\begin{align}
    \begin{split}
        \dot{\mathbf{x}} = \mathbf{A}\mathbf{x} + \mathbf{B}\mathbf{u}\,, \\
        \mathbf{y} = \mathbf{C}\mathbf{x} + \mathbf{D}\mathbf{u}\,,
    \end{split}
\end{align}
where $\mathbf{x} = \begin{pmatrix} \theta & \dot{\theta} & d & \dot{d} \end{pmatrix}^\top$ is the vector of the system's states and $\mathbf{u}$ the system's input torque. Deriving a second-order system from the matrices (\ref{eqn:BM_matrices}) and creating the state-space model, one can linearize the model around the equilibrium point $\mathbf{\tilde{x}} = \begin{pmatrix} 0 & 0 & 0 & 0 \end{pmatrix}^\top$. The enumerated state-space matrices of the linearized model are
\begin{align}
    \begin{split}
        \label{eqn:updated_matrices_balancing}
        \mathbf{A} &= 
            \begin{bmatrix}
            0 & 1 & 0 & 0 \\
            11.881 & -0.484 & -360.429 & -3.720 \\
            0 & 0 & 0 & 1 \\
            -7.073 & -0.166 & 2.060 & -16.847
            \end{bmatrix}, \quad
            \mathbf{B} = \begin{bmatrix} 0 \\  1123.9 \\ 0 \\ -6.423 \end{bmatrix}, \\
            \mathbf{C} &= \begin{bmatrix} 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \end{bmatrix}, \quad
            \mathbf{D} = \begin{bmatrix} 0 \\ 0 \end{bmatrix}.
    \end{split}
\end{align}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% REVOLVING MODE
\section{Revolving Mode} \label{sec:revolving}

For the revolving mode, a sketch of the ball in a revolving motion is shown in Figure \ref{fig:rev} with angles $\alpha$ and $\theta$ chosen as generalized coordinates having $\mathbf{q} = \begin{pmatrix} \theta & \alpha \end{pmatrix}^\top$. Again, angle $\theta$ is the angle of the beam w.r.t. to the horizontal axis of the world frame. Angle $\alpha$ is the angle of the ball center position w.r.t. the center of the beam's rounded part. \par
The ball position on one of the beam's rounded parts can be determined with the generalized coordinates, where the position of the rounded part's center is labeled with coordinates $x_1$ and $y_1$. Knowing this position, one can then define the ball center position with coordinates $x_2$ and $y_2$. Both positions are highlighted with a light blue mark in the figure.
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=6.5cm]{./img/revolving_ball.pdf}
    \captionsetup{justification=centering}
    \caption{Revolving ball on the beam denoted with the generalized coordinates $\theta$ and $\alpha$ and the moments of intertia $I_1$ and $I_2$.}
    \label{fig:rev}
\end{figure} \noindent
The computation of the coordinates is given by
\begin{align}
    \begin{aligned}
        x_1 &= \frac{d_1}{2}\cos(\theta)\,, \qquad &
        y_1 &= \frac{d_1}{2}\sin(\theta)\,, \\
        x_2 &= x_1 + (r_1 + r_2)\cos(\theta + \alpha)\,, \qquad & 
        y_2 &= y_1 + (r_1 + r_2)\sin(\theta + \alpha)\,.
    \end{aligned}
\end{align}
The ball angle $\varphi$ can be expressed as
\begin{equation}
    \varphi = \theta + \alpha\frac{r_1}{r_2}\,.
\end{equation}
The kinetic co-energy is
\begin{equation}
    T^* = \frac{1}{2}I_1\dot{\theta}^2 + \frac{1}{2}m_2(\dot{x}_2^2 + \dot{y}_2^2) + \frac{1}{2}I_2(\dot{\theta} + \dot{\varphi})^2\,.
\end{equation}
The potential energy is 
\begin{equation}
    V = m_2\mathrm{g}y_2\,.
\end{equation}
The dissipation function of the ball and the beam is
\begin{equation}
    D = \frac{1}{2}b_1\dot{\theta}^2 + \frac{1}{2}b_2\dot{\varphi}^2\,.
\end{equation}
Reckoning up the Lagrangian (\ref{eqn:lagrangian}), I can compute the Euler-Lagrange equations of the generalized coordinates $\theta$ and $\alpha$, similarly to (\ref{eqn:balancing1}) and (\ref{eqn:balancing2}). The matrices of the two differential equations describing the revolving motion are
\begin{align}
    \begin{split}
    \label{eqn:RM_matrices}
        \mathbf{M}(\mathbf{q}) &= 
        \begin{bmatrix}
            m_{11} & m_{12} \\ m_{21} & m_{22} 
        \end{bmatrix}\,, \\
        \mathbf{C}(\mathbf{q},\dot{\mathbf{q}}) &=
        \begin{bmatrix}
            b_1 + b_2 - m_2Rd_1\sin(\alpha)\dot{\alpha} & 
            b_2\dfrac{r_1}{r_2} - m_2R\dfrac{d_1}{2}\sin(\alpha)\dot{\alpha} \\
            b_2\dfrac{r_1}{r_2} + m_2R\dfrac{d_1}{2}\sin(\alpha)\dot{\theta} & 
            b_2\dfrac{r_1^2}{r_2^2}
        \end{bmatrix}\,, \\
        \mathbf{G}(\mathbf{q}, \dot{\mathbf{q}}) &=
        \begin{bmatrix}
            m_2\mathrm{g}\dfrac{d_1}{2}\cos(\theta) + m_2\mathrm{g}R\cos(\theta + \alpha) \\
            m_2\mathrm{g}R\cos(\theta + \alpha)
        \end{bmatrix}\,, \\
        \mathbf{Q} &=
        \begin{bmatrix}
            1 \\
            0
        \end{bmatrix}\tau\,,
    \end{split}
\end{align}
where 
\begin{align}
    \begin{split}
        R &= r_1 + r_2\,, \\
        m_{11} &= I_1 + 4I_2 + m_2R^2 + m_2\dfrac{d_1^2}{4} + m_2Rd_1\cos(\alpha)\,, \\
        m_{12} &= 2I_2\dfrac{r_1}{r_2} + m_2R^2 + m_2R\dfrac{d_1}{2}\cos(\alpha)\,, \\
        m_{21} &= 2I_2\dfrac{r_1}{r_2} + m_2R^2 + m_2R\dfrac{d_1}{2}\cos(\alpha)\,, \\
        m_{22} &= I_2\dfrac{r_1^2}{r_2^2} + m_2R^2\,.
    \end{split}
\end{align}

\subsection{Revolving Mode Linearization}

For the revolving mode, the required linearization of the state-space description using the matrices (\ref{eqn:RM_matrices}) is analogous to the balancing mode linearization. The vector of the system's states is $\mathbf{x} = \begin{pmatrix} \theta & \dot{\theta} & \alpha & \dot{\alpha} \end{pmatrix}^\top$ and the equilibrium point is $\mathbf{\tilde{x}} = \begin{pmatrix} \frac{\pi}{2} & 0 & 0 & 0 \end{pmatrix}^\top$. The enumerated state-space matrices of the linearized model are
\begin{align}
    \begin{split}
        \label{eqn:updated_matrices_revolving}
        \mathbf{A} &= 
            \begin{bmatrix}
            0 & 1 & 0 & 0 \\
            3.916 & 0.023 & -25.945 & 0.788 \\
            0 & 0 & 0 & 1 \\
            295.617 & -4.812 & 406.644 & -11.085
            \end{bmatrix}, \quad
            \mathbf{B} = \begin{bmatrix} 0 \\ 1092.3 \\ 0 \\ -4061.2 \end{bmatrix}, \\
            \mathbf{C} &= \begin{bmatrix} 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \end{bmatrix}, \quad
            \mathbf{D} = \begin{bmatrix} 0 \\ 0 \end{bmatrix}.
    \end{split}
\end{align}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% PROJECTILE MODE
\section{Projectile Mode} \label{sec:projectile}

For the projectile motion of the ball, a ball position $x$, $y$ and a ball angle $\varphi$ will be considered generalized coordinates meaning $\mathbf{q} = \begin{pmatrix} x & y & \varphi \end{pmatrix}^\top$. Angle $\varphi$ is the angle of the ball w.r.t. the vertical axis of the ball frame. \par
The kinetic co-energy is
\begin{equation}
    T^* = \frac{1}{2}m_2(\dot{x}^2 + \dot{y}^2) + \frac{1}{2}I_2\dot{\varphi}^2\,.
\end{equation}
The potential energy is given by 
\begin{equation}
    V = m_2\mathrm{g}y\,.
\end{equation}
This model will not consider the ball's air resistance in the free-fall motion. Because of the mass and the size of the metal ball, the air resistance can be omitted without any detriment to correctness resulting in $D = 0$. \par
With (\ref{eqn:lagrangian}), the Euler-Lagrange equations are
\begin{align}
    m_2\ddot{x} = 0\,, \qquad m_2\ddot{y} + m_2\mathrm{g} = 0\,, \qquad I_2\ddot{\varphi} = 0\,.
    \label{eqn:projectile}
\end{align}
The equations (\ref{eqn:projectile}) can be modified to get their final form. The equations of the ball in the free-fall motion are thus given by
\begin{align}
    \label{eqn:proj_final}
    \ddot{x} = 0\,, \qquad \ddot{y} = -\mathrm{g}\,, \qquad \ddot{\varphi} = 0\,.
\end{align}
As I mentioned in the description of the hybrid system, the choice of the Cartesian coordinates for this mode signifies that linearization of the projectile mode is unnecessary because the equations (\ref{eqn:proj_final}) are already linear.
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=9.5cm]{./img/projectile_motion.pdf}
    \captionsetup{justification=centering}
    \caption{Projecting the ball on the beam denoted with the generalized coordinates $x$, $y$ and $\varphi$ and the moments of inertia $I_1$ and $I_2$.}
    \label{fig:proj}
\end{figure}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% COORDINATES TRANSFORMATION
\section{Coordinates Transformation} \label{sec:transformation}

Having computed the homography in \ref{sec:homography}, the origin of the world plane $\mathbf{O}$ is set to the center pivot of the beam. To control the Flying Ball and Beam system, one has to compute two transformations to obtain the generalized coordinates $\mathbf{q}$ necessary for the balancing, revolving, and projectile modes. \par
As the angle $\theta$ is obtained directly from the motor block in Simulink, the length $d$ is the only unknown coordinate for the balancing mode. The coordinate $d$ is considered to be a \textit{signed distance}, which is given by
\begin{equation}
    \label{eqn:dist_d}
    d = \begin{pmatrix} \cos(\theta) \\ \sin(\theta) \end{pmatrix}^\top \left(\begin{pmatrix} x \\ y \end{pmatrix} - \begin{pmatrix} P_x \\ P_y \end{pmatrix}\right)\,,
\end{equation}
where $x$ and $y$ are the Cartesian coordinates of the ball. The point $\mathbf{P}$ is calculated as
\begin{equation}
    \begin{pmatrix} P_x \\ P_y \end{pmatrix} = \mathbf{R} \begin{pmatrix} 0 \\ h_{12} \end{pmatrix}\,,
\end{equation}
where $\mathbf{R}$ is the 2-dimensional rotation matrix. For the revolving mode, the angle $\alpha$ is given by
\begin{equation}
    \label{eqn:alpha}
    \alpha = -\atan\left(\frac{x - P_x}{y - P_y}\right)\,,
\end{equation}
where
\begin{equation}
    \begin{pmatrix} P_x \\ P_y \end{pmatrix} = \mathbf{R} \begin{pmatrix} 0 \\ \dfrac{d_1}{2} \end{pmatrix}\,.
\end{equation}
The negative sign in (\ref{eqn:alpha}) is added because of the angle $\alpha$ orientation in Figure \ref{fig:rev}. No transformation is needed for the projectile mode because the ball position $x$ and $y$ provide the generalized coordinates. \par \pagebreak
The ball rotation $\varphi$ cannot be precisely determined or computed during the projectile motion as the camera's output is only 2-dimensional. Still, the rotation of the ball could be estimated, which is, however, out of the scope of this work. 

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% TRANSITIONING BETWEEN MODES
\section{Transitioning between Modes} \label{sec:transitions}

The hybrid system was implemented using a \textit{Stateflow diagram} in Simulink. Stateflow (SF) provides a graphical language that includes state transition diagrams, flow charts, state transition tables, and truth tables. One can use Stateflow to describe how Matlab algorithms and Simulink models react to input signals, events, and time-based conditions \cite{Stateflow}. \par
The SF diagram of the hybrid system consists of three states, which correspond to the three modes of the Flying Ball and Beam system. The states are represented by the state-space description of the modes, while a set of guard conditions triggers the transitions between the states (see Figure \ref{fig:hybrid}) analyzed below. Every transition includes the guard condition and generalized coordinates transformations between the leaving and the upcoming state. The initial values of the upcoming state's transformed coordinates are marked with index $0$.
\begin{itemize}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Transition $\gamma_{12}$} -- The system switches from the balancing mode to the revolving mode the moment the ball reaches the end of the beam's flat part, where the ball starts the revolving motion. The modes transition is executed if the condition $\gamma_{12}$ is satisfied:
\begin{align}
\gamma_{12}(\mathbf{q}) &= \abs{d} - \dfrac{d_1}{2} > 0\,, \\
\alpha_0 &= \pm \dfrac{\pi}{2}\,, \nonumber
\end{align}
where the sign of $\alpha_0$ depends on the direction of the ball's movement. If the ball moves towards the right end of the beam, the distance $d$ is positive, and the sign of $\alpha_0$ is positive, too. For the ball moving towards the left, it is vice versa. The angle $\theta$ remains unchanged.}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Transition $\gamma_{21}$} -- The system switches from the revolving mode back to the balancing mode as soon as the ball finishes circumscribing the rounded part of the beam and returns to the flat part of the beam. The modes transition is executed if the condition $\gamma_{21}$ is satisfied:
\begin{align}
\gamma_{21}(\mathbf{q}) &= \abs{\alpha} - \dfrac{\pi}{2} > 0\,, \\
d_0 &= \pm \dfrac{d_1}{2}\,, \nonumber
\end{align}
where the sign of $d_0$ depends on the direction of the beam's rotation. If the beam rotates clockwise, the sign of $d_0$ is positive, and vice versa. The angle $\theta$ remains unchanged.}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Transition $\gamma_{13}$} -- As the description of the projectile mode states, the ball can be projected from any point on the beam's flat part. As soon as the ball approaches the desired point, where the ball is released, the system switches from balancing to projectile mode. The modes transition is executed if the condition $\gamma_{13}$ is satisfied:
\begin{align}
\gamma_{13}(\mathbf{q}) &= d + s - d_{p} > 0\,, \\
x_{0} &= d\cos(\theta) - h_{12}\sin(\theta)\,, \nonumber \\
y_{0} &= d\sin(\theta) + h_{12}\cos(\theta)\,. \nonumber
\end{align}
where $d_{p}$ is the distance between the desired release point and the center ball position on the beam. The distance $s$ is a non-zero threshold, which signifies the approach distance to the release point.}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Transition $\gamma_{31}$} -- When the ball approaches the beam after reaching the apex point of the projectile motion, the system switches from the projectile mode back to the balancing mode. This switch sets in right before contact between the ball and the beam emerges. The modes transition is executed if the condition $\gamma_{31}$ is satisfied:
\begin{align}
\label{eqn:pls}
\gamma_{31}(\mathbf{q}) &= s - p > 0\,, \\ 
d_0 &= x\,, \nonumber \\
\theta_0 &= 0\,, \nonumber
\end{align}
where $p$ is the distance between the beam and the ball, and $s$ is a non-zero threshold. Calculating a minimum distance between a line segment and a point represents the distance $p$ between the ball and the beam. The line segment corresponds to the flat part of the beam, and the point is the ball position. The calculation of this distance is done by using vectors \cite{PLS_dist}. \par
When the ball lands back on the beam, one can presuppose that the beam angle would be close to zero, which signifies
\begin{align}
    \theta \approx 0 \qquad \Longrightarrow \qquad d \approx x\,.
\end{align}
}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\end{itemize}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% MODES SIMULATION
\section{Modes Simulation} \label{sec:hybrid_simulation}

To simulate the individual modes of the hybrid system and verify the hybrid system's correctness, I intended to use Runge-Kutta 4th and 5th order method. Beginning with the balancing mode, I tried to simulate the nonlinear model of the mode described with the state-space vector. \par
To simulate the model, I used \texttt{ode45} with a time span of the system's sampling time. The simulation, however, proved to be unsuccessful as the simulation did not finish. To simulate the model, I then utilized \textit{Euler-Lagrange tool} package\footnote{The package can be downloaded from MathWorks File Exchange site: \url{https://www.mathworks.com/matlabcentral/fileexchange/49796-euler-lagrange-tool-package}.}, which enables one to generate a state-space vector of the model from a kinetic co-energy, potential energy, and the dissipation function. Using this tool, the simulation of the model emerged to be successful; for the mathematical model of the balancing mode, both frictions $b_1$ and $b_2$ were fine-tuned to match the real model. A comparison of the simulated mathematical model and the real model responses to an initial state $\mathbf{x}_{\rm{BM},0} = \begin{pmatrix} 0 & 0 & d & 0 \end{pmatrix}^\top$, where $d = 30\,\si{\milli\metre}$, is shown in Figure \ref{fig:bal_sim}. At the beginning of the simulation, the ball is placed on the beam to the distance $d$. The beam overbalances because of zero input torque, and the ball rolls down the groove.
\begin{figure}[H]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[trim=45 328 75 328, clip, width=9cm]{./img/bal_01.pdf}
        \captionsetup{justification=centering}
        \caption{Ball distance $d$.}
        \label{fig:ball_dist}
    \end{subfigure}\\
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[trim=45 328 75 328, clip, width=9cm]{./img/bal_02.pdf}
        \captionsetup{justification=centering}
        \caption{Ball velocity $\dot{d}$.}
        \label{fig:ball_vel}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[trim=45 328 75 328, clip, width=9cm]{./img/bal_03.pdf}
        \captionsetup{justification=centering}
        \caption{Beam angle $\theta$.}
        \label{fig:beam_ang}
    \end{subfigure}\\
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[trim=45 328 75 328, clip, width=9cm]{./img/bal_04.pdf}
        \captionsetup{justification=centering}
        \caption{Beam angular velocity $\dot{\theta}$.}
        \label{fig:beam_vel}
    \end{subfigure}
    \captionsetup{justification=centering}
    \caption{Comparison of the simulated mathematical model and the real model responses to the initial state $\mathbf{x}_{\rm{BM},0}$.}
    \label{fig:bal_sim}
\end{figure} \noindent
The simulation verifies the mathematical model of the balancing mode compared to the real model. While the ball distance $d$ and beam angle $\theta$ correspond, the difference in the ball velocity $\dot{d}$ and the beam angular velocity $\dot{\theta}$ of the mathematical and real model appears to be more significant than I initially assumed. This might be because the mathematical model is not entirely accurate in describing the ball and beam motion as it relies on simplifications and approximations in defining the exact mode's dynamical behavior. The second reason for this difference could be an improperly chosen simulation. Using the input torque in the simulation and simulating the ball motion on the beam could demonstrate the model's behavior more accurately. \par
Simulating the revolving mode, I wanted to verify the model's behavior for the initial state $\mathbf{x}_{\rm{RM},0} = \begin{pmatrix} \frac{\pi}{2} & 0 & 0 & 0 \end{pmatrix}^\top$ with no input torque as this state represents an unstable equilibrium of the model. First, the angles and angular velocities of the ball and the beam are zero and remain constant. In $t = 2.5\,\si{\second}$, the ball and the beam leave the equilibrium, ending up in a chaotic motion. This behavior was expected, as the model reflects only the ball's motion with the ball angle $\alpha \in [-\frac{\pi}{2}, \frac{\pi}{2}]$. Because of this uncertainty in the model's correctness, the graphs are not presented. \par
The simulation of the projectile mode is more straightforward as the model is linear. For the simulation of the projectile mode, the initial state is $\mathbf{x}_{\rm{PM},0} = \begin{pmatrix} 0 & 0 & y & 0 & 0 & 0 \end{pmatrix}^\top$, where $y = 0.1\,\si{\metre}$. Omitting the ball's air resistance, the model only depends on the gravitational acceleration $g$. The mathematical mode simulation shows the ball's free-fall motion in Figure \ref{fig:ball_freefall}. 
\begin{figure}[H]
    \centering
    \includegraphics[trim=45 328 75 328, clip, width=9cm]{./img/pro_01.pdf}
    \captionsetup{justification=centering}
    \caption{Simulated ball projectile motion response to the initial state $\mathbf{x}_{\rm{PM},0}$.}
    \label{fig:ball_freefall}
\end{figure}
