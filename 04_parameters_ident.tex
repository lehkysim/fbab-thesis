\chapter{Parameters Identification} \label{chapter:params_identification}

Having measured parameters describing the Flying Ball and Beam system enumerated in Table \ref{tab:parameters}, I will now determine the remaining unknown parameters: motor friction $b_1$ and ball friction $b_2$. I will also verify the motor torque by comparing the input reference torque with the real torque spinning the motor with the beam.

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% MOTOR AND BEAM FRICTION
\section{Motor and Beam Friction}

Regarding the ball friction coefficient $b_2$, I consider the same ball friction previously computed for the Flying Ball in Hoop since I use an identical ball as in the Flying Ball in Hoop system, and the beam is printed from the same material as the hoop. The friction $b_2$ is thus given by
\begin{equation}
    b_2 = 2.574 \cdot 10^{-6}\,\si{\newton\metre\second\per\radian}\,.
\end{equation}
The friction of the Moteus motor $b_1$ is left to be determined. For its estimation, I used a method called \textit{Grey-Box Model Estimation} in Matlab, which enables one to estimate coefficients of linear and nonlinear differential equations. Considering the motor friction linear, I derived the differential equation representing the motor from a bond graph\footnote{The bond graph was generated using a LaTeX Bond Graph Package created by Martin Gurtner. The package is available at \url{https://github.com/martingurtner/mgbondgraph}.}, as shown in Figure \ref{fig:bond_motor}.
\begin{figure}[H]
    \centering
    \begin{tikzpicture}[node distance = 2.3cm, font=\small, auto,  >=stealth']
        \node (Se) {$S_{e}$};
        \node (J)[right of=Se] {$1$};
        \node (I)[below of=J] {$I:I_1$};
        \node (R)[right of=J] {$R:b_1$};

        \bondCSEnd{Se}{J}{$\tau$}{$\times$};
        \bondCSEnd{J}{I}{$\dot{p}$}{$\dfrac{p}{I_1}$};
        \bondCSStart{J}{R}{$b_1\dfrac{p}{I_1}$}{$\dfrac{p}{I_1}$};
    \end{tikzpicture}
    \captionsetup{justification=centering}
    \caption{Bond graph model of the motor.}
    \label{fig:bond_motor}
\end{figure} \noindent
The differential equation is derived from the bond graph as
\begin{equation}
    \label{eqn:motor_eqn}
    \dot{p} = -\frac{b_1}{I_1}p + \tau\,,
\end{equation}
where $p$ is generealized momentum and $\tau$ the input torque. Firstly, I had to create a rectangular waveform of various input torques $\tau \in [-0.05, 0.05]\,\si{\newton\metre}$, which spun the motor. Considering the equation (\ref{eqn:motor_eqn}), I decided to estimate not only the motor friction $b_1$ but also the moment of inertia of the beam $I_1$. \par
Recording the input torque waveform and the spinning motor's corresponding velocity in Simulink, the two parameters were estimated using \texttt{idnlgrey} and \texttt{nlgreyest} functions. Firstly, \texttt{idnlgrey} created a linear grey-box model of the motor and \texttt{nlgreyest} then estimated the two coefficients. \par
After that, I used a different set of input torques to simulate the motor model with the estimated parameters using \texttt{ode45}, a numerical method for solving differential equations, and to spin the hardware motor. Comparing the velocities from the simulation and the real data, I then slightly adjusted the parameters' values. The motor friction $b_1$ and moment of inertia of the beam $I_1$ are thus given by
\begin{equation}
    \label{eqn:ident_params}
    b_1 = 15.893 \cdot 10^{-4}\,\si{\newton\metre\second\per\radian} \qquad \rm{and} \qquad I_1 = 8.860 \cdot 10^{-4}\,\si{\kilo\gram\metre\squared}\,.
\end{equation}
A comparison of the velocities from the simulation and the recorded real data is shown in Figure \ref{fig:friction}. \par
The inaccuracy in the estimation is likely caused since I approximated the friction with a linear parameter to model the motor and did not consider cogging torque characteristic of a brushless DC motor. The model is, however, helpful in estimating the behavior of the Moteus motor and assessing the unknown parameters. Regardless of the simplified model, I will consider the estimated parameters to enumerate nonlinear models of the hybrid system developed in Chapter \ref{chapter:hybrid}. \par

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% MOTOR TORQUE VERIFICATION
\section{Motor Torque Verification}

In this section, I want to verify that the real torque, which spins the motor, corresponds to the input reference torque. I compare these two quantities with a digital force gauge named SHITO FS-20N. \par
To compute the torque $\tau$ from the force of the motor $F$ measured by the force gauge, I consider an equation
\begin{equation}
    \label{eqn:torque}
    \tau = Fl\sin(\psi)\,,
\end{equation}
where $l$ is the distance measured from the beam center pivot to a point where the force is applied to the sensor of the force gauge. Angle $\psi$ is the angle between the beam and the gauge's sensor. I used a rectangular waveform with different amplitudes $\tau \in [0.02, 0.7]\,\si{\newton\metre}$ as a reference torque. The lower limit of the interval is the minimum torque required to spin the motor. Torque $\tau = 0.01\,\si{\newton\metre}$ was too low and could not move the motor. The upper limit $\tau = 0.7\,\si{\newton\metre}$ was chosen because I could no longer hold the beam and carry out the force measurement with the device for higher torques. \par
The force was measured for $l = 0.06\,\si{\metre}$ with the beam in the vertical position so that $\psi = \frac{\pi}{2}$. I verified that the measured force corresponds to the reference torque. The slight inaccuracy is likely caused by the force gauge not being perfectly rectangularly aligned with the beam while measuring. The comparison of the reference and measured torque is shown in Figure \ref{fig:torque}.
\begin{figure}[H]
    \centering
    \includegraphics[trim=55 270 75 285, clip, width=9.5cm]{./img/motor_fric.pdf}
    \captionsetup{justification=centering}
    \caption{Comparison of the simulated model's velocity determined using the identified parameters and the real motor's velocity.}
    \label{fig:friction}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[trim=55 270 75 285, clip, width=9.5cm]{./img/motor_tor.pdf}
    \captionsetup{justification=centering}
    \caption{Comparison of the input reference torque and the real torque measured with the force gauge.}
    \label{fig:torque}
\end{figure}
