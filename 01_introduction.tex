\chapter{Introduction} \label{chapter:introduction}

The ball and beam system is a classic and well-known problem in control engineering, widely used as a benchmark for developing and evaluating control strategies. The system has been described by a well-known control systems researcher Peter E. Wellstead, who has extensively studied different control systems, including the ball and beam system. He has made significant contributions to its analysis and control; see \cite{BaB_Wellstead} and \cite{System_modeling}. \par
Wellstead describes the ball and beam system as an example of a control system that provides a simple and intuitive way to understand the principles of feedback control while representing a difficult open-loop unstable control problem. The system, depicted in Figure \ref{fig:model_bab}, consists of a beam linked directly to a motor shaft or a linkage connecting the beam and a cam driven by a motor. The ball rolls freely on the beam, and the ball's position is controlled by varying the angle of the beam. \par
Another well-known system described by Wellstead is the ball and hoop system \cite{BaH_Wellstead}. The system, seen in Figure \ref{fig:model_bah}, contains a ball that can freely rotate within a hoop connected to a motor that can apply torque to it. The system represents another interesting control system rich in dynamics and can be, e.g., used to study the dynamics of certain liquid slosh problems. \par
Based on the ball and hoop design, a system called \textit{Flying Ball in Hoop} was created at FEE CTU and originally developed by the AA4CC\footnote{AA4CC, which stands for Advanced Algorithms for Control and Communication, is an academic research group fostered by the Department of Control Engineering at FEE CTU.} group members Jiří Zemánek and Martin Gurtner in 2017. \par
The Flying Ball in Hoop system\footnote{More information about the Flying Ball in Hoop system, including the system description and its setup, can be found at \url{https://aa4cc.github.io/flying-ball-in-hoop}.} introduced a more complex task named \textit{flying mode} on top of the relatively simple control of the ball in the hoop. Controlling the hoop's rotation, the ball could fly inside the hoop, demonstrating a more comprehensive range of system modeling and control design possibilities in control engineering. \par
This bachelor's thesis aims to expand upon this existing laboratory model. The \textit{Flying Ball and Beam} system, which replaces the hoop with a beam attached to the motor, is heavily based on the Flying Ball in Hoop system and serves as the master model from which this work proceeds from \cite{FBiH}. 
\begin{figure}[H]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[trim=30 10 25 10, clip, width=6cm]{./img/model_bab.pdf}
        \captionsetup{justification=centering}
        \caption{Ball and beam.}
        \label{fig:model_bab}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[trim=30 10 30 10, clip, width=6cm]{./img/model_bah.pdf}
        \captionsetup{justification=centering}
        \caption{Ball in hoop.}
        \label{fig:model_bah}
    \end{subfigure}
    \captionsetup{justification=centering}
    \caption{Two examples of control systems studied and described by Peter Wellstead.}
    \label{fig:models}
\end{figure} \noindent
The main objective of this thesis is to create a mathematical model for the Flying Ball and Beam system, which will incorporate three control modes and form a hybrid system. The follow-up aims are to design a controller for balancing the ball in two modes on the beam and projecting the ball from one place to another within the beam based on a camera used to track the ball's position. The Flying Ball in Hoop is photographed in Figure \ref{fig:system_bah} while the Flying Ball and Beam can be seen in Figure \ref{fig:system_bab} in Section \ref{sec:structure} on the next page. \par
Resembling the two examples with the flying ball is a similar system called Butterfly robot \cite{Butterfly}, whose task is to stabilize a ball in periodic motions on a beam suggestive of a butterfly's wings. A similar balancing task could also be considered a ball on plate system \cite{Ball_plate}, where the ball is balanced on a plate that can be tilted along axis $x$ and $y$ using a camera in the same way. \par
Instructions on setting up and using the Flying Ball and Beam system are described in the system's documentation, available in the Flying Ball and Beam GitHub repository. Links to all software source codes I use in this work, including the thesis repository, can be found in Appendix \ref{chapter:additional}.
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=12cm]{./img/system_bah.jpg}
    \captionsetup{justification=centering}
    \caption{Flying Ball in Hoop system developed at FEE.}
    \label{fig:system_bah}
\end{figure}
