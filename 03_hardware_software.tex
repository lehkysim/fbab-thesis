\chapter{Hardware and Software} \label{chapter:wares}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% HARDWARE
\section{Hardware} \label{sec:hardware}

The Flying Ball and Beam system consists of a baseboard with a motor holder and an electronics box. The board is a laser-cut plexiglass board coated with matte black paint. A controller and a motor from the MJBOTS company are powered by $24\,\rm VDC$ and are mounted on a holder made of PET plastic. A beam that can freely rotate is attached directly to the motor shaft. The beam has a groove around its whole perimeter in which a metal ball rolls. Two rubber bands are stretched around the perimeter on both sides of the beam serving as high-friction rails and preventing the ball from bouncing in the groove. \par
The electronics box is a wooden case built from individual rectangular sides and houses a Raspberry Pi, powered by $5\,\rm VDC$ via a USB-C cable. The box also includes a small area to store the ball with additional ones. A plexiglass is used as a cover keeping the box away from possible dust. The last component of the system is a Raspberry camera attached to the box's front side and connected to the Raspberry. \par
A scheme in Figure \ref{fig:components} shows how these key hardware components are connected, forming the Flying Ball and Beam system. Details about the electronic components are mentioned below. 
\begin{itemize} 
    \item{\textbf{Raspberry Pi}} -- The fundamental system constituent is the Raspberry Pi Model 4 B with a 32-bit Raspberry Pi OS installed. I discovered that a 64-bit operating system could not be used as it does not support the \texttt{picamera} package in Python, which is necessary for the RaspiCam camera operation. The Raspberry is connected to the user's computer by an ethernet cable. One can then communicate with the Raspberry via an SSH connection from the computer's terminal window. Due to heating issues, the passive cooling on the Raspberry was insufficient, and I had to add a Raspberry Armor Fan case to cool the device.
    \item{\textbf{MJBOTS motor}} -- For spinning the beam, I use an MJBOTS Moteus high-performance 5208 brushless motor attached to the motor mount on the baseboard.
    \item{\textbf{MJBOTS controller}} -- The motor is controlled using a controller named MJBOTS Moteus r4.11. The communication between the Raspberry and the Moteus motor is arranged by an MJBOTS FDCANUSB device, which provides a USB 2.0 interface to a CAN-FD bus. This setup connects the controller to the Raspberry Pi via a USB-A port.
    \item{\textbf{RaspiCam}} -- The RaspiCam module consists of a Raspberry Pi camera and two rings of LEDs. Twenty-four LEDs, powered by $48\,\rm VDC$, illuminate the scene, resulting in a better input image. The camera is connected to the Raspberry Pi via a ribbon cable. 
\end{itemize}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% MODIFICATION
\subsection{System Modification} \label{sec:modification} 

Initially, the Flying Ball and Beam system fully adopted the hardware setup from the Flying Ball in Hoop platform except for the beam. It used a brushless motor and a controller from the company ODrive to spin the beam. \par
The motor should have been able to be controlled in three different modes: \textit{positional}, \textit{velocity}, and \textit{current} mode. Even with the help of the ODrive documentation and its online community discourse with various tutorials, I could not resolve how to enable the velocity or current mode. I could only control the motor in the positional mode, which allows one to set a position the motor is supposed to reach. Being unable to use the motor in the current mode could potentially introduce significant limitations in future system control, so I had to find an alternative for the ODrive gear. \par
Eventually, I replaced the original gear with an MJBOTS motor and controller used in other AA4CC projects. In this case, I could easily set up both positional and current modes in the controller and control the motor. \par
 The motor change meant I had to redesign the beam, as the Moteus motor has different mounting points than the original motor. I designed the beam in Fusion 360, a 3D CAD software developed by Autodesk. The beam design can be found in the system's repository with an illustration in Appendix \ref{chapter:additional}.
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=7.9cm]{./img/system_components.pdf}
    \captionsetup{justification=centering}
    \caption[Scheme of the system]{Scheme of the system's architecture with its principal components.\footnotemark}
    \label{fig:components}
\end{figure}
\footnotetext{Raspberry and MJBOTS company logo images were downloaded from their websites \url{https://www.raspberrypi.com/trademark-rules} and \url{https://mjbots.com}.}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% SOFTWARE
\section{Software} \label{sec:software}

The principal software tools employed in this work are Matlab and Simulink. Matlab is used to develop a mathematical model of the Flying Ball and Beam system, model visualization, and identification of parameters. Simulink is used to design and implement control for the ball and beam system. Using Simulink, one connects to the Raspberry, which controls the system. \par
For a Simulink--Raspberry communication, I use the ERT Linux, which stands for \textit{Minimalist Simulink Coder Target for Linux}. This software, created at FEE, permits Simulink-generated code to be compiled, deployed, and launched on a Linux device - in this case, on the Raspberry Pi. \par
For motor control, I take advantage of a Moteus API in Simulink devised by Loi Do. The interface includes a System Objects block which allows the user to control the motor within the Simulink workspace. \par
Lastly, based on image processing, I use RaspiBallPos in Python to detect the ball's position created at FEE. RaspiBallPos, previously utilized in the Flying Ball in Hoop system, enables one to use a System Objects block in Simulink to obtain a real-time ball position from the image. \par
The Raspberry can be shut down with a button and a Python script, enabling the Raspberry shutdown service introduced in the Flying Ball in Hoop system. The button is connected to the Raspberry via a GPIO pin.

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% SIMULATION SETTING
\section{Simulation Setting} \label{sec:sim_set}

The sampling time of the RaspiCam, including the image processing, is set to $30\,\si{\milli\second}$. Initially, I assumed that the whole system's simulation had to be performed using the same sampling time. However, the control of the balancing tasks was far from fast and smooth with this sampling time. \par
I then discovered that the simulation sampling time could be chosen independently on the RaspiCam, meaning that the system would receive the same ball's position for the duration of one sampling step of the RaspiCam. The sampling time set to $1\,\si{\milli\second}$ was too high and caused aliasing. The simulation frequency was thus empirically set to $5\,\si{\milli\second}$.

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% RASPICAM SETTING
\section{RaspiCam Setting} \label{sec:raspicam_set}

Before using the RaspiCam, it is necessary to calibrate the camera to capture the ball's position on the beam properly. After turning on the camera with a command in the Raspberry terminal window, the camera is calibrated in a web interface. In the interface, the user can set the number of objects appearing in the image, including their parameters, such as radius. Then, one can adjust the color and white balance of the objects in the image while watching a real-life masked output image from the camera.

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% HOMOGRAPHY
\section{Camera Homography} \label{sec:homography}

The output of the RaspiCam is pixel coordinates $x$ and $y$ of the ball's position in the image with the origin pixel $[0,0]$ in the bottom right corner and resolution of $480\,\rm{x}\,480\,\rm{px}$. The Raspberry camera delivers the image upside down, resulting in having the origin pixel in the image's bottom right corner. \par
I used the camera homography $\mathbf{H}$ to map the image coordinates to the world coordinates. The homography is calculated based on a set of calibration points, which are the coordinates on the image plane $\mathbf{x}$ and their corresponding world coordinates $\mathbf{X}$ complying 
\begin{equation}
    \mathbf{X} = \mathbf{H}\mathbf{x}\,.
\end{equation}
The calibration points are depicted in Figure \ref{fig:homography}. For the homography calculation, I used a homogeneous estimation method based on singular value decomposition (SVD) \cite{Homography}. \par
Since the beam can rotate, only two colorful markers were individually stuck on one of the two projection points included on one side of the beam. The image plane coordinates were captured for $10\,\si{\second}$, then the median value was chosen as the output from the RaspiCam slightly fluctuated. The world coordinates were calculated using goniometric functions with the origin in the center pivot of the beam. Although only four points were necessary, I decided to get twelve of them to obtain better accuracy. The world coordinates were obtained by making combinations from a set of lengths $\mathcal{D} = \{30,60\}\,\si{\milli\metre}$ and a set of angles $\mathcal{A} = \frac{k\pi}{3},\,k\in\{0,...,5\}$. I used a Matlab script named \texttt{hom\_calib} available in the RaspiBallPos repository to get the homography.
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=8.5cm]{./img/homography.pdf}
    \captionsetup{justification=centering}
    \caption{Calibration points utilized for the homography with the beam in positions where the markers were captured.}
    \label{fig:homography}
\end{figure}
