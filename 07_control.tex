\chapter{Control Tasks} \label{chapter:control}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% CASCADE MOTOR CONTROL
\section{Cascade Motor Control} \label{sec:cascade_control}

As the subsection \ref{sec:modification} mentions, the motor can be controlled either in a positional or a current mode. Considering the positional mode, the input to the motor's Matlab System Object in Simulink is the desired angle $\theta$ of the beam, whereas, for the current control, one sets the input torque $\tau$. \par
The positional mode is, however, not convenient for control tasks such as balancing the ball. With this motor setting, I did not manage to control and balance the ball efficiently in any desired position on the beam. Therefore, I decided to switch to the current control mode. To control the motor in the current mode, one has to set the motor PID constants to zero during the Moteus controller configuration. The configuration is performed using the Moteus gui named \texttt{tview}, which lets one configure and inspect the state of the controller. \par
The current control requires setting a \textit{cascade control} structure to control the motor's torque, i.e., the moment of the motor, by choosing a reference position. The reference position is the beam angle $\theta$. The structure comprises two closed loops, both controlled by a PD controller, where the inner loop is a moment control loop, and the outer one is a speed control loop. \par
Initially, I individually implemented the inner loop with one controller and tuned its proportional and derivative constant. As reference velocities, I used sine and pulse waveforms with various amplitudes. After I set the inner loop controller, the outer loop with the second controller was added to the scheme and tuned likewise.

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% FLAT PART BALANCING
\section{Flat Part Balancing} \label{sec:balancing_flat}

The first task of the system control is balancing the ball in any position on the flat part of the beam so that $\theta = 0$. The system's architecture implemented in Simulink is depicted in Figure \ref{fig:loop1}, and all function blocks used in the control scheme are described in detail below. Balancing the ball in a desired position on the beam by following a reference position is displayed in Figure \ref{fig:balancing}.
\begin{figure}[t]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=12cm]{./img/balancing_loop.pdf}
    \captionsetup{justification=centering}
    \caption{Control loop for the flat part balancing.}
    \label{fig:loop1}
\end{figure} \noindent
\begin{itemize}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Coords Transform} -- The ball's Cartesian coordinates $x$ and $y$ are firstly transformed to a signed distance $d$ using the equation (\ref{eqn:dist_d}).}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Controller} -- For controlling the ball's position on the beam, I employed a PID controller with the following constants  
\begin{equation}
    K_p = 0.0031\,, \qquad K_i = 0.0021\,, \qquad K_d = 0.0029\,.
\end{equation}}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Estimator} -- As the Section \ref{sec:sim_set} states, the sampling time of the RaspiCam is $T_c = 30\,\si{\milli\second}$, while the system sampling time is $T_m = 5\,\si{\milli\second}$. The difference between the two sampling times means the ball position is updated in every sixth sample of the simulated system. With this setting, the beam could not effectively control the ball's position, as the beam tilting was not sufficiently smooth. \par
This issue was solved by introducing a linear estimator to the scheme \ref{fig:loop1}. For its functionality, I approximated the ball's motion on the beam with a linear motion. The velocity of the ball is calculated using the current position of the ball $d_c$ and the previous position $d_{c-1}$ from the camera
\begin{equation}
    \label{eqn:velocity}
    v = \frac{d_c - d_{c-1}}{T_c}\,.
\end{equation}
The five unknown ball positions between every two camera samples (position $d_0$ is the only known) are then calculated using an equation 
\begin{equation}
    d_i = d_c + ivT_m, \qquad i = 0,\ldots,5\,.
\end{equation}
The index $i$ is determined by a counter, which always counts to six with the system's sampling time and then resets back to zero.}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Moving Mean Filter} -- Considering the current and previous values of the ball distance $d_c$ and $d_{c-1}$, I discovered during the control design that some differences between the two values turned out to be too distinct. In some cases, velocity $v$ calculated from such a significant change in the ball position was overly large. The velocity used for the following five estimated distances did not then resemble the real velocity of the ball as the ball could be, in reality, slowed down by the beam. The inappropriately obtained velocity thus resulted in the wrong estimation of the ball position. \par
On that account, I added a moving mean to the control loop, which limits the vast distances of the ball estimated with the algorithm. This way, it uses the trend of the ball position and prevents the ball from moving too fast on the beam. \par
The moving mean is calculated from the last six distances corresponding to the length of one sample from the camera. The choice of the moving mean proved to be an efficient solution for an appropriate estimation of the ball position and was achieved by using delay blocks in Simulink. The equation of the moving mean is represented by
\begin{equation}
    u_{i+1} = \frac{1}{6} \sum_{j=0}^{5} u_{i-j}\,.
\end{equation}
The comparison of the position from the RaspiCam and the estimated position with the estimator and moving mean is shown in Figure \ref{fig:estimation}.}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\begin{figure}[H]
    \centering
    \includegraphics[trim=55 270 75 285, clip, width=9.5cm]{./img/estimation.pdf}
    \captionsetup{justification=centering}
    \caption{Comparison of the real ball position captured directly by the RaspiCam and the estimated position obtained using the estimator and the moving mean.}
    \label{fig:estimation}
\end{figure} \noindent
\begin{figure}[H]
    \centering
    \includegraphics[trim=55 270 75 290, clip, width=9.5cm]{./img/balancing.pdf}
    \captionsetup{justification=centering}
    \caption{Balancing the ball in various positions on the flat part of the beam by following a reference position.}
    \label{fig:balancing}
\end{figure} \noindent
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\item{\textbf{Dead Zone} -- Although the ball remains steady on the beam and does not move, the distance $d$ fluctuates $\pm 0.5\,\si{\milli\metre}$ around the reference position. The added dead zone prevents the motor from compensating for such small fluctuations as well as minor position differences if the ball does not exactly reach the reference position. The dead zone is thus given by
\begin{equation}
    \abs{d_{\rm ref} - d} < 1.5\,\si{\milli\metre}\,.
\end{equation}}
%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
\end{itemize}

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% ROUNDED PART BALANCING
\section{Rounded Part Balancing} \label{sec:balancing_rounded}

Balancing the ball on the beam's rounded part signifies that $\theta = \frac{\pi}{2}$ with the aim to control the system so that $\alpha = 0$, which is shown in Figure \ref{fig:rev}. The ball's coordinates $x$ and $y$ are transformed with (\ref{eqn:alpha}) to control the angle $\alpha$. \par
Barriers were added to the sides of the beam in the vertical position preventing the ball from falling. To control the $\alpha$ deviation, I designed a PID controller with a similar scheme architecture to the one used to balance the ball in the horizontal position. \par
However, the control was never efficient and fast enough to steadily hold the ball in the desired position. I wanted to ascertain thus that controlling the ball on the rounded part was feasible with this hardware. \par
At first, I measured the system's delay, which is the time it takes for the controller to react to the $\alpha$ deviation. To measure correctly the delay between capturing and processing the image from the RaspiCam to computing the regulator's control input, I switched the simulation sampling time back to the original value $30\,\si{\milli\second}$ and measured two significant times. \par
I used the motor in the positional mode with a reference position $d = 0\,\si{\milli\metre}$ and balanced the ball on the beam in its horizontal position. I then deflected the beam by hand and captured the time of the first angle change $\rm{d}\theta$, which is the motor block output. To get the second time, I measured the time of the first control input from the controller, corresponding to the motor input. By subtracting these two times, I got the system's delay, which is $90\,\si{\milli\second}$. \par
After that, I measured the time it took the ball to move from the tip of the beam's rounded part to a position where the controller could no longer control it. For that position, I chose an angle $\alpha \approx \frac{\pi}{3}$. This revolving motion was captured by a super slow-motion mode on a mobile phone camera. Using the OpenCV library in Python, I calculated the number of frames in the recorded video and divided it by the slow-motion mode frame rate of the mobile phone's camera, which is $960\,\si{\hertz}$ \cite{frames}. The resulting time was $145\,\si{\milli\second}$. This means that the system has to balance the ball on the beam's rounded part with only one sample of the control input provided by the controller. \par
This signifies that the system's hardware is unsuitable for such control task. Considering the system's delay, one has to take into account the Raspberry camera's frame rate, the time it takes to process the image, and the delay in the communication between the Raspberry and the Moteus controller and the motor. The most significant improvement would be to use a higher frame-rate camera and a faster image-processing algorithm. 

%-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-%
% BALL PROJECTING
\section{Ball Projecting} \label{sec:projecting}

The third control task is the projectile mode, where the ball is projected by tilting the beam. The ball performs a free-fall motion and lands back on the beam. For this task, I assume that the ball's motion can only be directly controlled while the ball is in contact with the beam. When the ball starts the free-fall motion, the motion of the ball is only affected by the gravity force. \par
The variable parameters of this task are three different positions of the beam. The first is the ball's initial position, and the second is the point where the ball is released from the beam and projected. The third parameter is the point where the ball is balanced after landing back on the beam. With these preconditions, I divided the projectile mode into five sequential motions. \par
The ball can be projected from any position on the beam; for the following description of the projectile mode, I assume that the ball is projected from the left half of the world plane, which implies a negative distance $d$. The five motions of the projectile mode are as follows:
\begin{itemize}
    \item{\textbf{Release point approach} -- The entire mode starts with the ball moving from the initial position toward the release point. This maneuver is executed by the same control algorithm for balancing the ball on the beam's flat part.}
    \item{\textbf{Ball projecting} -- Once the ball approaches the desired release point, the beam tilts slightly in the direction of the forthcoming projectile motion so that $\rm{d}\theta$ is negative. Such a tilt should prevent the ball from flying in the wrong direction and landing away from the beam because of the centrifugal force applied to the ball. After that, the motor rapidly tilts the beam, causing the beam to project the ball, which starts the free-fall motion.}
    \item{\textbf{Free-fall} -- After projecting the ball, the beam returns to the horizontal position so that $\theta = 0$, and the ball follows a parabolic trajectory.}
    \item{\textbf{Fall dampening} -- When the ball approaches the beam, it tilts in the direction of the ball's fall to dampen its impact. The function that calculates the distance between the ball and the beam was introduced in Section \ref{sec:transitions} and is implemented in \texttt{PLS\_dist.m}. The fall dampening is controlled using the equation (\ref{eqn:pls}).}
    \item{\textbf{Ball balancing} -- After the ball lands back on the beam, the ball is balanced in the desired position in the same way as in the balancing mode.}
\end{itemize}
The beam tilt, which projects the ball and the beam's subsequent return to the horizontal position, is set directly with a reference position waveform controlling the beam's position. The waveform is created as a fractional function composed of several linear functions corresponding to ramp functions. \par
The waveform is designed and can be adjusted by changing parameter $k$ in each linear function's equation $y = kx + q$ and the duration of every ramp. The script for generating such a waveform can be found in \texttt{create\_FF\_wav.m} and is depicted in Figure \ref{fig:rotation}. \par
The projectile task is implemented in Simulink. The control scheme consists of the motor block, camera block, and the particular motions of the projectile mode described above. The elements of the projectile mode are activated successively by three switches depending on the ball position. The logic behind the control of the mode is clarified with a diagram in Figure \ref{fig:projectile}. In the diagram, \texttt{Leave pos} and \texttt{End pos} is the mode's second and third parameters of the mode. Block \texttt{Switch2FF} controls switching between the \textit{Release point approach} motion and the \textit{Ball projecting} motion, as well as between the \textit{Free-fall}, \textit{Fall dampening} and \textit{Ball balancing} motion. For the diagram's simplicity, signals from the block \texttt{Switch2FF} triggering the switches are not shown in the figure. \par
Completing the task within the prescribed period proved unattainable in light of the time limitation, which resulted in being unable to test and debug the implemented projectile mode introduced and described above.
\begin{figure}[H]
    \centering
    \includegraphics[trim=0 0 0 0, clip, width=11.5cm]{./img/projectile_loop.pdf}
    \captionsetup{justification=centering}
    \caption{Diagram of the projectile mode control.}
    \label{fig:projectile}
\end{figure} 
\begin{figure}[H]
    \centering
    \includegraphics[trim=45 270 65 285, clip, width=9.5cm]{./img/ff_waveform.pdf}
    \captionsetup{justification=centering}
    \caption{Reference beam's position used to project the ball and level the beam in its horizontal position.}
    \label{fig:rotation}
\end{figure}
